# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import array, math
import tensorflow as tf
import numpy as np

from ROOT import TH1F, TH2F, TVirtualFitter, TCanvas

cc = TCanvas("c","", 600, 600)
cc.Divide(2, 2)

from rootpy.plotting import Hist, Hist2D
from timeit import default_timer as timer

fptype = tf.float64
ctype = tf.complex128

@tf.function
def Density(ampl) : return tf.abs(ampl)**2

@tf.function
def Complex(re, im) : return tf.complex(re, im)

@tf.function
def Const(c) : return tf.constant(c, dtype=fptype)

@tf.function
def Sqrt(x) : return tf.sqrt(x)

@tf.function
def Log(x):
    return tf.math.log(x)

@tf.function
def Pi():
    return Const(np.pi)

@tf.function
def CastComplex(re):
    """ Cast a real number to complex """
    return tf.cast(re, dtype=ctype)

# Masses of the initial and final state particles 
md  = 1.869
mpi = 0.139
mk  = 0.497

# Blatt-Weisskopf radial constants
dd = Const(5.)
dr = Const(1.5)


@tf.function
def HelicityAngle(m2ab, m2bc, md, ma, mb, mc) :
  md2 = md**2
  ma2 = ma**2
  mb2 = mb**2
  mc2 = mc**2
  m2ac = md2 + ma2 + mb2 + mc2 - m2ab - m2bc
  mab = tf.sqrt(m2ab)
  mac = tf.sqrt(m2ac)
  mbc = tf.sqrt(m2bc)
  eb = (m2ab-ma2+mb2)/2./mab
  ec = (md2-m2ab-mc2)/2./mab
  pb = tf.sqrt(eb**2-mb2)
  pc = tf.sqrt(ec**2-mc2)
  e2sum = (eb+ec)**2
  m2bc_max = e2sum-(pb-pc)**2
  m2bc_min = e2sum-(pb+pc)**2
  return (m2bc_max + m2bc_min - 2.*m2bc)/(m2bc_max-m2bc_min)

class DalitzPhaseSpace : 

  def __init__(self, ma, mb, mc, md) : 
    self.ma = ma
    self.mb = mb
    self.mc = mc
    self.md = md
    self.ma2 = ma**2
    self.mb2 = mb**2
    self.mc2 = mc**2
    self.md2 = md**2
    self.msqsum = self.md2 + self.ma2 + self.mb2 + self.mc2
    self.minab = (ma + mb)**2
    self.maxab = (md - mc)**2
    self.minbc = (mb + mc)**2
    self.maxbc = (md - ma)**2

  @tf.function
  def Inside(self, m2ab, m2bc) : 
    inside = tf.logical_and(tf.logical_and(tf.greater(m2ab, self.minab), tf.less(m2ab, self.maxab)), \
                            tf.logical_and(tf.greater(m2bc, self.minbc), tf.less(m2bc, self.maxbc)))
    eb = (m2ab - self.ma2 + self.mb2)/2./tf.sqrt(m2ab)
    ec = (self.md2 - m2ab - self.mc2)/2./tf.sqrt(m2ab)
    p2b = eb**2 - self.mb2
    p2c = ec**2 - self.mc2
    inside = tf.logical_and(inside, tf.logical_and(tf.greater(p2c, 0), tf.greater(p2b, 0)))
    m2bc_max = (eb+ec)**2 - (tf.sqrt(p2b) - tf.sqrt(p2c))**2
    m2bc_min = (eb+ec)**2 - (tf.sqrt(p2b) + tf.sqrt(p2c))**2
    return tf.logical_and(inside, tf.logical_and(tf.greater(m2bc, m2bc_min), tf.less(m2bc, m2bc_max) ) )

  def UniformSample(self, size, majorant = -1) : 
    if majorant>0 : 
      v = [ tf.random.uniform([size], self.minab, self.maxab, dtype=fptype), 
            tf.random.uniform([size], self.minbc, self.maxbc, dtype=fptype), 
            tf.random.uniform([size], 0., majorant, dtype=fptype) ] 
    else : 
      v = [ tf.random.uniform([size], self.minab, self.maxab, dtype=fptype), 
            tf.random.uniform([size], self.minbc, self.maxbc, dtype=fptype) ] 
    dlz = tf.stack(v, axis=1)
    return tf.boolean_mask(dlz, self.Inside(v[0], v[1]) )

  @tf.function
  def M2ab(self, sample) : return sample[:,0]

  @tf.function
  def M2bc(self, sample) : return sample[:,1]

  @tf.function
  def M2ac(self, sample) : return self.msqsum - self.M2ab(sample) - self.M2bc(sample)

  @tf.function
  def HelicityAB(self, sample) : 
    return HelicityAngle(self.M2ab(sample), self.M2bc(sample), self.md, self.ma, self.mb, self.mc)

  @tf.function
  def HelicityBC(self, sample) : 
    return HelicityAngle(self.M2bc(sample), self.M2ac(sample), self.md, self.mb, self.mc, self.ma)

  @tf.function
  def HelicityAC(self, sample) : 
    return HelicityAngle(self.M2ac(sample), self.M2ab(sample), self.md, self.mc, self.ma, self.mb)


@tf.function
def RelativisticBreitWigner(m2, mres, wres):
    """
    Relativistic Breit-Wigner 
    """
    if wres.dtype is ctype :
#        return 1./(CastComplex(mres*mres - m2) - Complex(Const(0.), mres)*wres)
        return (CastComplex(mres*mres - m2) - Complex(Const(0.), mres)*wres)**(-1)
    if wres.dtype is fptype :
#        return 1./Complex(mres*mres - m2, -mres*wres)
        return Complex(mres*mres - m2, -mres*wres)**(-1)
    return None

#@tf.function
#def RelativisticBreitWigner(m2ab, mres, wres) :
#  return Complex(mres**2-m2ab, -mres*wres)**(-1)

@tf.function
def HelicityAmplitude(x, spin) : 
  if spin == 0 : return Complex(Const(1.), Const(0.))
  if spin == 1 : return Complex(x, Const(0.))
  if spin == 2 : return Complex(x**2-1./3., Const(0.))
  if spin == 3 : return Complex(x**3-3./5.*x, Const(0.))
  return None

def AcceptRejectSample(density, phsp, size, majorant) : 
  sample = phsp.UniformSample(size, majorant)
  x = sample[:,:-1]
  r = sample[:,-1]
  return tf.boolean_mask(x, density(x)>r)

def EstimateMaximum(density, phsp, size) : 
  return tf.reduce_max( density(phsp.UniformSample(size)) )

__all_variables__ = []

class FitParameter : 
  def __init__(self, name, init_value, step_size, lower_limit, upper_limit) : 
    global __all_variables__
    self.var = tf.Variable(init_value, dtype = fptype, trainable = True)
    __all_variables__ += [ self ]
    self.init_value = init_value
    self.par_name = name
    self.step_size = step_size
    self.lower_limit = lower_limit
    self.upper_limit = upper_limit
    self.prev_value = None

  def update(self, value) : 
    if value != self.prev_value : 
      self.var.assign(value)
      self.prev_value = value

  def floating(self) : return self.step_size > 0

@tf.function
def UnbinnedLogLikelihood(pdf, data_sample, integ_sample) :
  norm = tf.reduce_sum(pdf(integ_sample))
  return -tf.reduce_sum(tf.math.log(pdf(data_sample)/norm ))


#@tf.function
def ZemachTensor(m2ab, m2ac, m2bc, m2d, m2a, m2b, m2c, spin):
    """Zemach tensor for 3-body D->ABC decay

    :param m2ab: 
    :param m2ac: 
    :param m2bc: 
    :param m2d: 
    :param m2a: 
    :param m2b: 
    :param m2c: 
    :param spin: 

    """
    if spin == 0:
        return Complex(Const(1.), Const(0.))
    if spin == 1:
        return Complex(m2ac-m2bc+(m2d-m2c)*(m2b-m2a)/m2ab, Const(0.))
    if spin == 2:
        return Complex((m2bc-m2ac+(m2d-m2c)*(m2a-m2b)/m2ab)**2-1./3.*(m2ab-2.*(m2d+m2c) +
                       (m2d-m2c)**2/m2ab)*(m2ab-2.*(m2a+m2b)+(m2a-m2b)**2/m2ab), Const(0.))
    return None

#@tf.function
def BlattWeisskopfFormFactor(q, q0, d, l):
    """
    Blatt-Weisskopf formfactor for intermediate resonance
    """
    z = q*d
    z0 = q0*d

    def hankel1(x):
        if l == 0:
            return Const(1.)
        if l == 1:
#            return 1 + x**2
            return 1 + x*x
        if l == 2:
#            x2 = x**2
            x2 = x*x
            return 9 + x2*(3. + x2)
        if l == 3:
#            x2 = x**2
            x2 = x*x
            return 225 + x2*(45 + x2*(6 + x2))
        if l == 4:
#            x2 = x**2
            x2 = x*x
            return 11025. + x2*(1575. + x2*(135. + x2*(10. + x2)))
    return Sqrt(hankel1(z0)/hankel1(z))

@tf.function
def TwoBodyMomentum(md, ma, mb):
    """Momentum of two-body decay products D->AB in the D rest frame

    :param md: 
    :param ma: 
    :param mb: 

    """
    return Sqrt((md**2-(ma+mb)**2)*(md**2-(ma-mb)**2)/(4*md**2))


@tf.function
def ComplexTwoBodyMomentum(md, ma, mb):
    """Momentum of two-body decay products D->AB in the D rest frame.
      Output value is a complex number, analytic continuation for the
      region below threshold.

    :param md: 
    :param ma: 
    :param mb: 

    """
    return Sqrt(Complex((md**2-(ma+mb)**2)*(md**2-(ma-mb)**2)/(4*md**2), Const(0.)))

@tf.function
def MassDependentWidth(m, m0, gamma0, p, p0, ff, l):
    """
    Mass-dependent width for BW amplitude
    """
#    return gamma0*((p/p0)**(2*l+1))*(m0/m)*(ff**2)
    if l == 0 : return gamma0*(p/p0)*(m0/m)*(ff*ff)
    if l == 1 : return gamma0*((p/p0)**3)*(m0/m)*(ff*ff)
    if l == 2 : return gamma0*((p/p0)**5)*(m0/m)*(ff*ff)
    if l >= 3 : return gamma0*((p/p0)**(2*l+1))*(m0/m)*(ff**2)


@tf.function
def OrbitalBarrierFactor(p, p0, l):
    """
    Orbital barrier factor
    """
#    return (p/p0)**l
    if l == 0 : return Ones(p)
    if l == 1 : return (p/p0)
    if l >= 2 : return (p/p0)**l

@tf.function
def BreitWignerLineShape(m2, m0, gamma0, ma, mb, mc, md, dr, dd, lr, ld, barrierFactor=True, ma0=None, md0=None):
    """
    Breit-Wigner amplitude with Blatt-Weisskopf formfactors, mass-dependent width and orbital barriers
    """
    m = Sqrt(m2)
    q = TwoBodyMomentum(md, m, mc)
    q0 = TwoBodyMomentum(md if md0 is None else md0, m0, mc)
    p = TwoBodyMomentum(m, ma, mb)
    p0 = TwoBodyMomentum(m0, ma if ma0 is None else ma0, mb)
    ffr = BlattWeisskopfFormFactor(p, p0, dr, lr)
    ffd = BlattWeisskopfFormFactor(q, q0, dd, ld)
    width = MassDependentWidth(m, m0, gamma0, p, p0, ffr, lr)
    bw = RelativisticBreitWigner(m2, m0, width)
    ff = ffr*ffd
    if barrierFactor:
        b1 = OrbitalBarrierFactor(p, p0, lr)
        b2 = OrbitalBarrierFactor(q, q0, ld)
        ff *= b1*b2
    return bw*Complex(ff, Const(0.))
    #return RelativisticBreitWigner(m2, m0, gamma0)

@tf.function
def FlatteLineShape(s, m, g1, g2, ma1, mb1, ma2, mb2):
    """
      Flatte line shape
        s : squared inv. mass
        m : resonance mass
        g1 : coupling to ma1, mb1
        g2 : coupling to ma2, mb2
    """
    mab = Sqrt(s)
    pab1 = TwoBodyMomentum(mab, ma1, mb1)
    rho1 = 2.*pab1/mab
    pab2 = ComplexTwoBodyMomentum(mab, ma2, mb2)
    rho2 = 2.*pab2/CastComplex(mab)
    gamma = (CastComplex(g1**2*rho1) + CastComplex(g2**2)*rho2)/CastComplex(m)
    return RelativisticBreitWigner(s, m, gamma)

@tf.function
def GounarisSakuraiLineShape(s, m, gamma, m_pi):
    """
      Gounaris-Sakurai shape for rho->pipi
        s     : squared pipi inv. mass
        m     : rho mass
        gamma : rho width
        m_pi  : pion mass
    """
    m2 = m*m
    m_pi2 = m_pi*m_pi
    ss = Sqrt(s)

    ppi2 = (s-4.*m_pi2)/4.
    p02 = (m2-4.*m_pi2)/4.
    p0 = Sqrt(p02)
    ppi = Sqrt(ppi2)

    hs = 2.*ppi/Pi()/ss*Log((ss+2.*ppi)/2./m_pi)
    hm = 2.*p0/Pi()/m*Log((m+2.*ppi)/2./m_pi)

    dhdq = hm*(1./8./p02 - 1./2./m2) + 1./2./Pi()/m2
    f = gamma*m2/(p0**3)*(ppi2*(hs-hm) - p02*(s-m2)*dhdq)

    gamma_s = gamma*m2*(ppi**3)/s/(p0**3)

    dr = m2-s+f
    di = ss*gamma_s

    r = dr/(dr**2+di**2)
    i = di/(dr**2+di**2)

    return Complex(r, i)

@tf.function
def Resonance(x, m, w, spin, ch) : 
    if ch == 0 : 
      return BreitWignerLineShape(dlz.M2ab(x), m,  w, mpi, mk, mpi, md, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2ab(x), dlz.M2ac(x), dlz.M2bc(x), Const(md**2), Const(mpi**2), Const(mk**2), Const(mpi**2), spin)
    if ch == 1 : 
      return BreitWignerLineShape(dlz.M2bc(x), m,  w, mk, mpi, mpi, md, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2bc(x), dlz.M2ab(x), dlz.M2ac(x), Const(md**2), Const(mk**2), Const(mpi**2), Const(mpi**2), spin)
    if ch == 2 : 
      return BreitWignerLineShape(dlz.M2ac(x), m,  w, mpi, mpi, mk, md, dr, dd, spin, spin, barrierFactor = False)*\
               ZemachTensor(dlz.M2ac(x), dlz.M2bc(x), dlz.M2ab(x), Const(md**2), Const(mpi**2), Const(mpi**2), Const(mk**2), spin)
      #return BreitWignerLineShape(dlz.M2ac(x), m,  w, mpi, mpi, mk, md, dr, dd, spin, spin, barrierFactor = False)
    if ch == 3 : 
      return GounarisSakuraiLineShape(dlz.M2ac(x), m,  w, mpi)*\
             ZemachTensor(dlz.M2ac(x), dlz.M2bc(x), dlz.M2ab(x), Const(md**2), Const(mpi**2), Const(mpi**2), Const(mk**2), spin)
    if ch == 4 : 
      return FlatteLineShape(dlz.M2ac(x), m, 0.09, 0.02, mpi, mpi, mk, mk)*\
             ZemachTensor(dlz.M2ac(x), dlz.M2bc(x), dlz.M2ab(x), Const(md**2), Const(mpi**2), Const(mpi**2), Const(mk**2), spin)
    return None

def MinuitFit(model, data_sample, integ_sample, call_limit = 50000) :

  tfpars = __all_variables__
  float_pars = [ p for p in tfpars if p.floating() ]
  float_tfpars = [ p.var for p in float_pars ]

  def fcn(npar, gin, f, par, istatus) :
    for i,p in enumerate(float_pars) : p.update(par[i])
    if istatus == 2 :            # If gradient calculation is needed
      with tf.GradientTape() as gradient : 
        gradient.watch(float_tfpars)
        nll = UnbinnedLogLikelihood(model, data_sample, integ_sample)
      dnll = gradient.gradient(nll, float_tfpars, unconnected_gradients=tf.UnconnectedGradients.ZERO)
      for i in range(len(float_tfpars)) : gin[i] = dnll[i] # Pass gradient to MINUIT
    else : 
      nll = UnbinnedLogLikelihood(model, data_sample, integ_sample)
    f[0] = nll
    fcn.n += 1
    if fcn.n % 10 == 0 : print(fcn.n, istatus, f[0], float_tfpars )

  fcn.n = 0
  minuit = TVirtualFitter.Fitter(0, len(float_tfpars))
  minuit.Clear()
  minuit.SetFCN(fcn)
  arglist = array.array('d', 10*[0])

  for n,p in enumerate(float_pars) : 
    minuit.SetParameter(n, p.par_name, p.init_value, p.step_size, p.lower_limit, p.upper_limit)

  minuit.ExecuteCommand("SET GRA", arglist, 0)
  arglist[0] = call_limit
  minuit.ExecuteCommand("MIGRAD", arglist, 1)

if __name__ == "__main__" : 

  dlz = DalitzPhaseSpace(0.139, 0.497, 0.139, 1.869)

  uniform_sample = dlz.UniformSample(2000000)

  # Initial complex amplitudes for resonance components, (ampl, phase)
  a  = []
  a += [ (2.666864, 160.480162) ]

  a += [ (1.560805, 214.065284) ]
  a += [ (0.491463,  64.390750) ]
  a += [ (0.034337, 111.974402) ]
  a += [ (0.0385497, 207.278721) ]
  a += [ (0.203222, 212.128769) ]
  a += [ (1.436933, 342.852060) ]
  a += [ (1.561670, 109.586718) ]

  a += [ (1.638345, 133.218073) ]
  a += [ (0.651326, 119.929357) ]
  a += [ (2.209476, 358.855281) ]
  a += [ (0.890176, 314.789317) ]
  a += [ (0.877231,  82.271754) ]

  a += [ (0.149579, 325.356816) ]
  a += [ (0.423211, 252.523919) ]
  a += [ (0.364030,  87.118694) ]
  a += [ (0.228236, 275.203595) ]
  a += [ (2.081620, 130.047574) ]

  # Transform initial values of amplitudes into Cartesian fit parameters 
  c = []
  for n,ai in enumerate(a) : 
    c += [ ( FitParameter("a%dr" % n, ai[0]*math.cos(ai[1]*math.pi/180.), 0.01, -10., 10.).var,
             FitParameter("a%di" % n, ai[0]*math.sin(ai[1]*math.pi/180.), 0.01, -10., 10.).var ) ]

  #@tf.function
  @tf.function(experimental_relax_shapes=True)
  def model(x) :
    ampl  = Complex(Const(0.), Const(0.))

    ampl += Complex(c[0][0], c[0][1])

    ampl += Complex(Const(1.), Const(0.))*Resonance(x, Const(0.775500), Const(0.149400), 1, 3)
    ampl += Complex(c[1][0], c[1][1])*Resonance(x, Const(0.522477), Const(0.453106), 0, 2)

    ampl += Complex(c[2][0], c[2][1])*Resonance(x, Const(1.0),      Const(0.4550),   1, 2)
    ampl += Complex(c[3][0], c[3][1])*Resonance(x, Const(0.782650), Const(0.008490), 1, 2)
    ampl += Complex(c[4][0], c[4][1])*Resonance(x, Const(0.97700),  Const(0.1),      0, 4)
    ampl += Complex(c[5][0], c[5][1])*Resonance(x, Const(1.033172), Const(0.087984), 0, 2)
    ampl += Complex(c[6][0], c[6][1])*Resonance(x, Const(1.275400), Const(0.185200), 2, 2)
    ampl += Complex(c[7][0], c[7][1])*Resonance(x, Const(1.434000), Const(0.173000), 0, 2)

    ampl += Complex(c[8][0], c[8][1])*Resonance(x, Const(0.891660),  Const(0.050800), 1, 0)
    ampl += Complex(c[9][0], c[9][1])*Resonance(x, Const(1.414000),  Const(0.232000), 1, 0)
    ampl += Complex(c[10][0], c[10][1])*Resonance(x, Const(1.414000), Const(0.290000), 0, 0)
    ampl += Complex(c[11][0], c[11][1])*Resonance(x, Const(1.425600), Const(0.098500), 2, 0)
    ampl += Complex(c[12][0], c[12][1])*Resonance(x, Const(1.717000), Const(0.322000), 1, 0)

    ampl += Complex(c[13][0], c[13][1])*Resonance(x, Const(0.891660), Const(0.050800), 1, 1)
    ampl += Complex(c[14][0], c[14][1])*Resonance(x, Const(1.414000), Const(0.232000), 1, 1)
    ampl += Complex(c[15][0], c[15][1])*Resonance(x, Const(1.414000), Const(0.290000), 0, 1)
    ampl += Complex(c[16][0], c[16][1])*Resonance(x, Const(1.425600), Const(0.098500), 2, 1)
    ampl += Complex(c[17][0], c[17][1])*Resonance(x, Const(1.717000), Const(0.322000), 1, 1)

    return Density( ampl )

  tf.random.set_seed(1)

  majorant = EstimateMaximum(model, dlz, 1000000)*1.1
  print("Maximum = ", majorant)

  gen_sample = AcceptRejectSample(model, dlz, 100000, majorant)

  start = timer()
  MinuitFit(model, gen_sample, uniform_sample)
  end = timer()

  fit_sample = AcceptRejectSample(model, dlz, 10000000, majorant)

  print(len(gen_sample), len(uniform_sample), len(fit_sample))

  h1 = Hist2D(100, dlz.minab-0.2, dlz.maxab+0.2, 100, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h2 = Hist2D(100, dlz.minab-0.2, dlz.maxab+0.2, 100, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h3 = Hist(100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h4 = Hist(100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h5 = Hist(100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 
  h6 = Hist(100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 

  h1.fill_array(gen_sample.numpy()[:,(0,1)])
  h3.fill_array(gen_sample.numpy()[:,0])
  h5.fill_array(gen_sample.numpy()[:,1])

  h2.fill_array(fit_sample.numpy()[:,(0,1)])
  h4.fill_array(fit_sample.numpy()[:,0])
  h6.fill_array(fit_sample.numpy()[:,1])

  h4.SetLineColor(2)
  h6.SetLineColor(2)
  cc.cd(1); h1.Draw("zcol")
  cc.cd(2); h2.Draw("zcol")
  cc.cd(3); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
  cc.cd(4); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
  cc.Update()

  print(end-start)
