import tensorflow as tf

import sys
sys.path.append("../lib/")

from ROOT import TH1F, TH2F, TCanvas
from Interface import *
from Kinematics import *
from Dynamics import *
from Optimisation import *

sess = tf.Session()
init = tf.initialize_all_variables()
sess.run(init)

x = [0., 0., 0., 0., 0.]
y = [1., 1., 1., 1., 1.]
z = [2., 2., 2., 2., 2.]
t = [3., 3., 3., 3., 3.]

tx = Const(x)
ty = Const(y)
tz = Const(z)
tt = Const(t)
p4 = LorentzVector(Vector(tx, ty, tz), tt)
p3 = SpatialComponents(p4)

print sess.run( tx )
print sess.run( ty )
print sess.run( tz )
print sess.run( tt )
print sess.run( p4 )
print sess.run( p3 )
